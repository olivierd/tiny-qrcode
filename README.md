# What is Tiny QR Code

It is lightweight Python3 script, which generates a QR code image. It uses
the Python library, [qrcode](https://github.com/lincolnloop/python-qrcode).

GUI version is currently in development.
