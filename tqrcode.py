#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright (c) 2018, Olivier Duchateau <duchateau.olivier@gmail.com>
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
# OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


'''
Lightweight Python script, which builds a QR code image.
'''

import argparse
import io
import pathlib
import sys
import uuid

try:
    import qrcode
except ImportError as e:
    print(e)
    sys.exit(-1)


class TinyQrCode(object):

    def __init__(self, fileobj, output=None):

        if isinstance(fileobj, io.TextIOWrapper):
            # We read only one line!
            self.data = fileobj.readline()

            self.__err = qrcode.constants.ERROR_CORRECT_M
            # Size of QR Code (here 25x25 matrix)
            self.__size = 2

            # `img_path' is a pathlib.Path object
            if output is None:
                self.img_path = self.tqrc_set_output()
            else:
                self.img_path = self.tqrc_ensure_png_format(output)

    def tqrc_ensure_png_format(self, filename):
        p = pathlib.Path(filename).resolve()

        if p.suffix != '.png':
            return p.with_suffix('.png')
        else:
            return p

    def tqrc_set_output(self, prefix='qrcode'):
        p = pathlib.Path.home()

        return p.joinpath('{0}-{1}.png'.format(prefix, uuid.uuid4()))

    def tqrc_render(self):
        qr = qrcode.QRCode(error_correction=self.__err,
                           version=self.__size,
                           box_size=10)
        qr.add_data(self.data)
        qr.make(fit=False)

        # Output
        img = qr.make_image()
        img.save(self.img_path, 'PNG')


def check_file_is_empty(filename):
    p = pathlib.Path(filename).resolve()

    if p.stat().st_size == 0:
        print('Error: file is empty')
        sys.exit(-1)


def main(args):
    check_file_is_empty(args.input[0].name)

    q = TinyQrCode(args.input[0], output=args.output)
    q.tqrc_render()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-o', '--output',
                        help='Final QR Code image')
    parser.add_argument('-i', '--input', nargs=1, required=True,
                        type=argparse.FileType('r'),
                        help='Data to store in QR Code')
    main(parser.parse_args())
